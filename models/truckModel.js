const mongoose = require('mongoose');

const truckSchema = mongoose.Schema({
  created_by: {
    type: String,
    required: true,
  },
  assigned_to: {
    type: String,
    default: ''
  },
  type: {
    type: String,
    required: true,
  },
  status: {
    type: String,
    required: true,
    default:'IS',
  }, 
  createdDate: {
      type: Date,
      default: Date.now()
  },
  width: {
    type: Number,
    required: true,
  },
  length: {
    type: Number,
    required: true,
  },
  height: {
    type: Number,
    required: true,
  },
  payload: {
    type: Number,
    required: true,
  }
});

module.exports.Truck = mongoose.model('Truck', truckSchema);