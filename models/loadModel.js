const mongoose = require('mongoose');

const loadSchema = mongoose.Schema({
  created_by: {
    type: String,
    required: true,
  },
  assigned_to: {
    type: String,
    default: null
  },
  status: {
    type: String,
    required: true,
    default: 'NEW'
  }, 
  state: {
    type: String,
    required: true,
    default: 'En route to Pick Up'
  },
  name: {
    type: String,
    required: true,
  },
  payload: {
    type: Number,
    required: true,
  },
  pickup_address: {
    type: String,
    required: true,
  },
  delivery_address: {
    type: String,
    required: true,
  },
  dimensions: {
    type: Object,
    required: true,
    width: {
      type: Number,
      required: true,
    },
    length: {
      type: Number,
      required: true,
    },
    height: {
      type: Number,
      required: true,
    }
  },
  logs: {
    type: Array,
    default: []
  },

  createdDate: {
      type: Date,
      default: Date.now()
  }
});

module.exports.Load = mongoose.model('Load', loadSchema);