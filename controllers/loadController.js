const {Load} = require('../models/loadModel')
const {Truck} = require('../models/truckModel')

const getUsersLoads = async(req, res) => {
  let loads

  if (req.user.role === 'SHIPPER') {

    const {offset, limit} = req.query;
    loads = await Load.find({created_by: req.user._id}, {__v: 0}).skip(offset).skip(limit)

  } else if (req.user.role === 'DRIVER') {
    loads = await Load.find({assigned_to: req.user._id})
  }

  if (!loads) {
    res.status(400).json({message: 'loads was not found'})
  }

  res.status(200).json({loads});
}
  

const addLoadForUser = async(req, res) => {
  const created = req.user._id;

  const {name, payload, pickup_address, delivery_address, dimensions} = req.body;

  const load = new Load({
    created_by: created,
    name,
    payload,
    pickup_address, 
    delivery_address, 
    dimensions
  })

  await load.save()
  res.status(200).json({message: "Load created successfully"})
}

const getUsersActiveLoad = async(req, res) => {
  const activeLoad = await Load.findOne({assigned_to: req.user._id, status: {$in: ['ASSIGNED', 'SHIPPED']}});
  if (!activeLoad) {
    res.status(400).json({message: 'There is no active load'})
  }
  res.status(200).json({load: activeLoad})

}

const iterateToNextState = async(req, res) => {
  if (req.user.role === 'DRIVER') {
    const load = await Load.findOne({assigned_to: req.user._id, status: {$in: [ 'SHIPPED', 'ASSIGNED', ]}})

    if (!activeLoad) {
      res.status(400).json({message: 'There is no active load'})
    }
    
    const currentState = load.state;
    const stateArr = [ 'En route to Pick Up', 'Arrived to Pick Up', 'En route to delivery',' Arrived to delivery' ]
    const newState = stateArr[stateArr.indexOf(currentState) + 1]

    if(newState === 'En route to delivery') {
      await Load.updateOne(activeLoad, {$set: {status: 'SHIPPED'}})
    }
    await Load.updateOne(activeLoad, {$set: {state: newState}})
    res.status(200).json({message: `Load state changed to '${newState}'`})
  }
}

const getUsersLoad = async(req, res) => {
  const load = await Load.findOne({created_by: req.user._id, _id: req.params['id']}, {__v: 0})
  if (!load) {
    res.status(400).json({message: 'No load was found'})
  }
  res.status(200).json({load})
}

const updateUsersLoad = async(req, res) => {
  const load = await Load.findOne({created_by: req.user._id, _id: req.params['id']}, {__v: 0})
  if (!load) {
    res.status(400).json({message: 'No load was found'})
  }

  if(load.status !== 'NEW') {
    res.status(400).json({message:` You can update only 'NEW' loads `})
  }
  await Load.updateOne(load, req.body)
  res.status(200).json({message: 'Load details changed successfully'})

}

const deleteUsersLoad = async(req, res) => {
  const load = await Load.findOne({created_by: req.user._id, _id: req.params['id']}, {__v: 0})
  if (!load) {
    res.status(400).json({message: 'No load was found'})
  }

  if(load.status !== 'NEW') {
    res.status(400).json({message:` You can delete only 'NEW' loads `})
  }
  await Load.deleteOne(req.note)
  res.status(200).json({message: 'Load deleted successfully'})
}



const postUsersLoad = async(req, res) => {

  const load = await Load.findOne({created_by: req.user._id, _id: req.params.id}, {__v: 0})

  if (!load) {
    res.status(400).json({message: 'No load was found'})
  }
  await Load.updateOne(load, {$set: {status:'POSTED'}})

  const findTruck = await Truck.findOne({
    status: {$eq: 'IS'}, 
    assigned_to: {$ne: ''},
    payload: {$gt: load.payload},
    length: {$gt: load.dimensions.length},
    width: {$gt: load.dimensions.width},
    height: {$gt: load.dimensions.height},
  })

  // if(!findTruck) {
  //   res.status(400).json({message:` No trucks was found `})
  //   await Load.updateOne(load, {$set: {status:'NEW'}})
  // } else {
    await Load.updateOne(load, {$set: {status:'ASSIGNED', assigned_to: findTruck.assigned_to}})
    await Truck.updateOne(findTruck, {$set: {status:'OL'}})
  // }



  res.status(200).json({ 
    "message": "Load posted successfully",
    "driver_found": true})
}


const getShippingInfo = async(req, res) => {
  const load = await Load.findOne({created_by: req.user._id, _id: req.params['id']}, {__v: 0})
  if (!load) {
    res.status(400).json({message: 'No load was found'})
  }
  if (!load.assigned_to) {
    res.status(200).json({load})
  }
  const truck = await Truck.findOne({assigned_to: load.assigned_to})
  res.status(200).json({load, truck})

}

module.exports = {
  getUsersLoads, 
  addLoadForUser, 
  getUsersActiveLoad, 
  iterateToNextState, 
  getUsersLoad,
  updateUsersLoad, 
  deleteUsersLoad, 
  postUsersLoad,
  getShippingInfo
}

