const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');

const { JWT_SECRET } = require('../config');
const { User } = require('../models/userModel');

const registration = async (req, res) => {
  const { email, password, role } = req.body;
  const existUser = await User.findOne({email})
console.log(req.body)
  if(existUser) {
    return res.status(400).json({message: 'user is already exist'})
  }

  const user = new User({
    email,
    password: await bcrypt.hash(password, 10), 
    role
  });

  await user.save();

  res.json({message: 'Profile created successfully'});
};

const login = async (req, res) => {
  const { email, password } = req.body;
  const user = await User.findOne({email});

  if (!user) {
    return res.status(400).json({message: `No user with username '${email}' found!`});
  }

  if ( !(await bcrypt.compare(password, user.password)) ) {
    return res.status(400).json({message: `Incorrect password!`});
  }

  const token = jwt.sign({ email: user.email, _id: user._id, role: user.role }, JWT_SECRET);
  res.json({jwt_token: token});
};

const forgotPassword = async (req, res) => {
  const newPassword = generator.generate
}

module.exports = {registration, login, forgotPassword}