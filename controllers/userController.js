const {User} = require('../models/userModel')
const bcrypt = require('bcrypt')

const getUser = async (req, res) => {
    const user = await User.findOne({_id : req.user._id}, {password: 0, __v:0})

    if(!user) {
     res.status(400).json({message: 'user not found'})
    }
    res.status(200).json({user: {_id: user._id, email: user.email, createdDate: user.created_date}});

}

const deleteUser = async (req, res) => {
    await User.findByIdAndRemove(req.user._id, req.body, err => {
        if (!err) {
            res.status(200).json({message: 'Profile deleted successfully'})
        }
    })
}

const editUser = async (req, res) => {

  const {oldPassword, newPassword} = req.body;
  const user = await User.findById(req.user._id, 'password').exec();

  if(!(await bcrypt.compare(oldPassword, user.password))) {
    return res.status(400).json({message: 'incorrect password'});
  }
  user.password = await bcrypt.hash(newPassword, 10);
  await user.save()

  res.status(200).json({message: 'Password changed successfully' })
}

module.exports = {getUser, deleteUser, editUser}

