const {Truck} = require('../models/truckModel')

const types =  {
  'SPRINTER': {
    length: 300,
    width: 250,
    height: 170,
    payload: 1700,
  }, 
  'SMALL STRAIGHT':{
    length: 500,
    width: 250,
    height: 170,
    payload: 2500,
  },
  'LARGE STRAIGHT': {
    length: 700,
    width: 350,
    height: 250,
    payload: 4000,
  }} 

const getUsersTrucks = async (req, res) => {
  const trucks = await Truck.find({created_by: req.user._id}, {__v: 0})
  if(!trucks) {
    res.status(400).json({message: 'no truck was found'})
  }
  res.status(200).json({trucks})
}

const addTruckForUser = async (req, res) => {
  const types =  {
    'SPRINTER': {
      length: 300,
      width: 250,
      height: 170,
      payload: 1700,
    }, 
    'SMALL STRAIGHT':{
      length: 500,
      width: 250,
      height: 170,
      payload: 2500,
    },
    'LARGE STRAIGHT': {
      length: 700,
      width: 350,
      height: 250,
      payload: 4000,
    }} 
  const {length, width, height, payload} = types[req.body.type]

  const truck = new Truck({
    created_by: req.user._id,
    type: req.body.type,
    length: length,
    width: width,
    height: height,
    payload: payload,
  }) 

  await truck.save()
  res.status(200).json({message: 'Truck created successfully'})

}

const getUsersTrack = async (req, res) => {
  const truck = await Truck.findOne({created_by: req.user._id, _id: req.params['id']}, {__v: 0})

  if (!truck) {
    res.status(400).json({message: 'No truck was found'})
  }
  res.status(200).json({truck})

}

const updatetUsersTrack = async (req, res) => {
  const truck = await Truck.findOne({created_by: req.user._id, _id: req.params['id']}, {__v: 0})
  if (!truck) {
    res.status(400).json({message: 'No truck was found'})
  }
  if (truck.assigned_to) {
    res.status(400).json({message: `You can't update assignet truck`})
  }

  await Truck.updateOne(truck, {
    type: req.body.type,
    length: types.req.body.type.length,
    width: types.req.body.type.width,
    height: types.req.body.type.height,
    payload: types.req.body.type.payload,
  }) 
  await truck.save()
  res.status(200).json({message: 'Truck details changed successfully'})
}

const deletetUsersTrack = async (req, res) => {
  const truck = await Truck.findOne({created_by: req.user._id, _id: req.params['id']}, {__v: 0})
  if (!truck) {
    res.status(400).json({message: 'No truck was found'})
  }
  if (truck.assigned_to) {
    res.status(400).json({message: `You can't delete assignet truck`})
  }
  await Truck.deleteOne(truck)
  res.status(200).json({message: 'Truck deleted successfully'})

}

const assignTruck = async (req, res) => {
  const truck = await Truck.findOne({created_by: req.user._id, _id: req.params.id}, {__v: 0})
console.log(truck)
  if (!truck) {
    res.status(400).json({message: 'No truck was found'})
  }
  await Truck.findOneAndUpdate({assigned_to: req.user._id}, {$set: {
    assigned_to: '',
  }})
  await Truck.findOneAndUpdate({_id: truck._id}, {$set: {
    assigned_to: req.user._id
  }})
  res.status(200).json({message: 'Truck assigned successfully'})

}

module.exports = {getUsersTrucks, addTruckForUser, getUsersTrack, updatetUsersTrack, deletetUsersTrack, assignTruck}