const express = require('express');
const router = new express.Router();
const {asyncWrapper} = require('./helpers')

const { 
    getUsersLoads, 
    addLoadForUser, 
    getUsersActiveLoad, 
    iterateToNextState, 
    getUsersLoad, 
    updateUsersLoad, 
    deleteUsersLoad, 
    postUsersLoad,
    getShippingInfo
} = require('../controllers/loadController');

const { authMiddleware } = require('./middlewares/authMiddleware');

router.get('/', authMiddleware, asyncWrapper(getUsersLoads))
router.post('/', authMiddleware, asyncWrapper(addLoadForUser))
router.get('/active', authMiddleware, asyncWrapper(getUsersActiveLoad))
router.patch('/active/state', authMiddleware, asyncWrapper(iterateToNextState))
router.get('/:id', authMiddleware, asyncWrapper(getUsersLoad))
router.put('/:id', authMiddleware, asyncWrapper(updateUsersLoad))
router.delete('/:id', authMiddleware, asyncWrapper(deleteUsersLoad))
router.post('/:id/post', authMiddleware, asyncWrapper(postUsersLoad))
router.get('/:id/shipping_info', authMiddleware, asyncWrapper(getShippingInfo))

module.exports = router