const express = require('express');
const router = new express.Router();
const {asyncWrapper} = require('./helpers')

const { getUsersTrucks, addTruckForUser, getUsersTrack, updatetUsersTrack, deletetUsersTrack, assignTruck} = require('../controllers/truckController');
const { authMiddleware } = require('./middlewares/authMiddleware');

router.get('/trucks', authMiddleware, asyncWrapper(getUsersTrucks))
router.post('/trucks', authMiddleware, asyncWrapper(addTruckForUser))
router.get('/trucks/:id', authMiddleware, asyncWrapper(getUsersTrack))
router.put('/trucks/:id', authMiddleware, asyncWrapper(updatetUsersTrack))
router.delete('/trucks/:id', authMiddleware, asyncWrapper(deletetUsersTrack))
router.post('/trucks/:id/assign', authMiddleware, asyncWrapper(assignTruck))

module.exports = router