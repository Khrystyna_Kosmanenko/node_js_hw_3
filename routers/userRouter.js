const express = require('express');
const router = new express.Router();
const {asyncWrapper} = require('./helpers')
const { authMiddleware } = require('./middlewares/authMiddleware');

const { getUser, deleteUser, editUser} = require('../controllers/userController');


router.get('/', authMiddleware, asyncWrapper(getUser))
router.delete('/', authMiddleware, asyncWrapper(deleteUser))
router.patch('/password', authMiddleware, asyncWrapper(editUser))

module.exports = router;
