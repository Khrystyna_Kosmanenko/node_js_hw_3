const jwt = require('jsonwebtoken');
const { JWT_SECRET } = require('../../config');

const authMiddleware = (req, res, next) => {
  const header = req.headers['authorization'];

  const [tokenType, token] = header.split(' ');

  if (!token) {
      return res.status(401).json({message: `No Authorization http header found!`});
  }

  if (!token) {
      return res.status(401).json({message: `No JWT token found!`});
  }

  
  try {
      req.user = jwt.verify(token, JWT_SECRET);
      next()
  } catch(err) {
      return res.status(400).json({message: 'Invalid token'})
  }
}

module.exports = { authMiddleware }
