const Joi = require('joi');

const validateRegistration = async (req, res, next) => {
    const schema = Joi.object({
        email: Joi.string()
            .email()
            .required(),
    
        password: Joi.string()
            .pattern(new RegExp('^[a-zA-Z0-9]{6,30}$'))
            .required(),
        
        role: Joi.string()
            .pattern(new RegExp('DRIVER|SHIPPER'))
            .required(),

    });
    try {
        await schema.validateAsync(req.body);
    } catch(e) {
        console.log('something wrong')
    }
    
    next()
}

module.exports = {validateRegistration}